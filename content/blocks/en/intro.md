---
---

For over a decade, the Open GLAM (Gallery, Library, Archive, Museum) movement has advocated open access to cultural heritage held in memory institutions, in order to promote the free exchange of ideas and to enable knowledge equity.

How many cultural heritage institutions today make their digital collections openly available? How do they do this and where is open access most common? Where are the gaps? This resource presents key insights and visualisations about Open GLAM, based on survey data that has been gathered by Douglas McCarthy and Dr Andrea Wallace since 2018.
