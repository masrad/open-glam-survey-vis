---
---

## Not all data is eligible

GLAMs take different approaches to releasing data. Some release individual datasets of collections as they are prepared for open access publication. Others adopt open access as a matter of policy and make all eligible data available at the same time, sometimes during a major digital overhaul like a new website or a content management system upgrade. There is no one right path to open access. Instead, there are many diverse routes to open GLAM participation.
