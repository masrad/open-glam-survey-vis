---
---

## Data sources

Large data aggregators and infrastructures are useful for sourcing information to populate this survey. We use data from platforms like Europeana, Wikimedia Commons, Japan Search and Trove to find instances of open GLAM. This can result in heavy representation in certain geographic areas, particularly where significant and sustained funding has been allocated to aggregation platforms. Where such platforms do not exist, it is often difficult to locate Open GLAM instances.