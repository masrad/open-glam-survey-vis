---
order: 2
---

# Public domain compliance

Whether rights arise is a legal question that varies from one country to another. The prevailing interpretation is that no new rights arise in the non-original media resulting from the reproduction of public domain works, like digital surrogates, basic data or metadata. In practice, many GLAMs continue to claim rights in this media, while releasing it under open licenses, like CC BY and CC BY-SA.
